/******************************************************************************
 * AUTHOR: Maria Vanessa Vengco <vengcom@oregonstate.edu>
 * DATE: November 4, 2015
 * FILENAME: converter.cpp
 * DESCRIPTION: Project 6.b - Two recursive functions: One that converts a
 * string of binary numbers into its decimal equivalent, and a second that
 * converts a positive base 10 integer into its binary equivalent.
 * ****************************************************************************/

#include <iostream>
#include <string>

// helper function prototype
int binToDec(std::string, int, int);

/******************************************************************************
 * FUNCTION:                       binToDec()
 * DESCRIPTION: This function converts a base 2 number of type string
 * (parameter) into a base 10 number of type integer (return type/value). 
 * ****************************************************************************/

int binToDec(std::string bin)
{
    return binToDec(bin, 0, 0);
}

/******************************************************************************
 * FUNCTION:        Overloaded helper function for binToDec()
 * DESCRIPTION: Takes 3 parameters: the binary string, an integer index for 
 * each character in the string, and an integer variable called dec to
 * hold the decimal value as each position value in the string is converted.
 * Starting at the most significant bit, we multiply dec by 2 then add the
 * binary value of the current index position. Then recursively do the same to 
 * every digit in the binary number until it is converted into base 10.
 * ****************************************************************************/

int binToDec(std::string bin, int pos, int dec)
{
    if (pos == bin.length())  // base case
    {
        return dec;
    }
    else if (bin.at(pos) == '1')
    {
        dec *= 2;
        return binToDec(bin, pos + 1, ++dec);
    }
    else    //if (bin.at(pos) == '0')
    {
        dec *= 2;
        return binToDec(bin, pos + 1, dec);
    }
}


/******************************************************************************
 * FUNCTION:                       decToBin()
 * DESCRIPTION: A function that takes as parameter a decimal number of type 
 * int, then uses modulus division to convert it into a binary number of type 
 * C++ string. 
 * ****************************************************************************/

std::string decToBin(int dec)
{
    if (dec/2 == 0)     // base case
        if (dec % 2 == 0)
            return "0";
        else            // dec % 2 == 1
            return "1";
    else if (dec % 2 == 0)
        return decToBin(dec/2) + "0";
    else                // dec % 2 == 1
        return decToBin(dec/2) + "1";
}




//int main()
//{
//    int decimal = 156;
//    std::cout << decimal << " to Base 2: " << decToBin(decimal) << std::endl;   // 10011100
//    std::cout << "1 to Base 2: " << decToBin(1) << std::endl;                   // 1
//    std::cout << "0 to Base 2: " << decToBin(0) << std::endl;                   // 0
//    std::cout << "2 to Base 2: " << decToBin(2) << std::endl;                   // 10
//    std::cout << "999 is Base 2: " << decToBin(999) << std::endl;               // 1111100111
//    std::string base2 = "1011001";      // 89
//
//    std::cout << binToDec(base2) << std::endl;
//    std::cout << binToDec("0") << std::endl;
//    std::cout << binToDec("1") << std::endl;
//    std::cout << binToDec("10") << std::endl;
//    std::cout << binToDec("10011100") << std::endl;
//    std::cout << binToDec("1111100111") << std::endl;
//
//    return 0;
//}
