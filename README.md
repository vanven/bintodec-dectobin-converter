# README #

### What is this repository for? ###

Solution for CS 165 Assignment. 

### Details: ###
**Project 6.b**

Write two recursive functions - one should take as a parameter a C++ string of '1's and '0's that are the binary representation of a positive integer, and return the equivalent int value; the other should take as a parameter a positive int value, and return a C++ string of '1's and '0's that are the binary representation of that number (no leading zeros).  The functions should be named binToDec and decToBin.  Do not use any number base conversion functionality that is built into C++.
 
The file must be named: converter.cpp
 
Conversion help:

[http://www.wikihow.com/Convert-from-Decimal-to-Binary](http://www.wikihow.com/Convert-from-Decimal-to-Binary)

[http://www.wikihow.com/Convert-from-Binary-to-Decimal](http://www.wikihow.com/Convert-from-Binary-to-Decimal)